#include "roller.h"

static unsigned int MAX_SUPPORT(const int supporters) {
    if(supporters<0)
        return 0;
    return (supporters>2) ? 2 : supporters;
}

roll_result roll(const unsigned int numberOfDice,
                 const roll_type successThreshold) {
    int tossedDice;
    roll_result result;
    result.crits=0;
    result.successes=0;
    for(tossedDice=0;tossedDice<numberOfDice;++tossedDice) {
        int dieValue = toss_die();
        result.crits += (dieValue==FACE_CRIT) ? 1 : 0;
        result.successes += (dieValue<=successThreshold) ? 1 : 0;
    }
    return result;
}

attack_result judge_attack(const roll_result attackRoll, const roll_result defendRoll) {
    if(attackRoll.successes==0)
        return attack_missed;
    if(defendRoll.crits > attackRoll.crits)
        return attack_thwarted;
    if(attackRoll.crits > defendRoll.crits)
        return attack_crit;
    if(attackRoll.successes > defendRoll.successes)
        return (attackRoll.crits>0) ? attack_crit : attack_hit;
    return attack_thwarted;
}
