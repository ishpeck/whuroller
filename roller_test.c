#include "roller.h"

#define DEFAULT_MAX_ATTEMPTS 20000000

static unsigned int numeric(const char *parseMe) {
    long long result = strtonum(parseMe, 0, DEFAULT_MAX_ATTEMPTS, NULL);
    return (unsigned int)result;
}

int show_help(const char *proggy, const char *extraMsg) {
    printf("Usage: %s [-s N] [-h N]\n", proggy);
    printf("    -b         Attack gains cleave on a crit.\n"
           "    -c         Attack has cleave.\n"
           "    -d N       Attack deals N damage if it hits.\n"
           "    -! N       Attack deals N extra damage if it's a crit.\n"
           "    -h N       Attack with N dice looking for hammers.\n"
           "    -s N       Attack with N dice looking for swords.\n"
           "    -D N       Defend with N dice looking for dodges.\n"
           "    -G N       Defend with N dice while on GUARD.\n"
           "    -S N       Defend with N dice looking for shields.\n");
    if(extraMsg==NULL)
        return 0;
    printf("%s\n", extraMsg);
    return 1;
}

int report_test_results(const unsigned int attempts,
                        const unsigned int successes,
                        const unsigned int crits,
                        const unsigned int misses,
                        const unsigned int thwarts,
                        const unsigned int damage) {
    if(attempts<1)
        return 1;
    printf("Results of %d attacks...\n", attempts);
    printf("     Hits: %f (%f rate of critical)",
           (double)successes/(double)attempts,
           (double)crits/(double)attempts);
    if(damage>0)
        printf(" %f damage per attack", (float)damage/(float)attempts);
    printf("\n");

    printf(" Failures: %f (%f rate of also driving back)\n",
           (double)misses/(double)attempts,
           (double)thwarts/(double)attempts);
    return 0;
}

int proceed_with_test(const unsigned int maxAttempts,
                      const unsigned int atkType,
                      const unsigned int atkDiceQuant,
                      const unsigned int atkDmg,
                      const unsigned int critDmg,
                      const unsigned int defType,
                      const unsigned int defDiceQuant) {
    unsigned int attempts,
        successes=0,
        crits=0,
        misses=0,
        thwarts=0,
        damageDealt=0;
    for(attempts=0;attempts<maxAttempts;++attempts) {
        switch(judge_attack(roll(atkDiceQuant, atkType),
                            roll(defDiceQuant, defDiceQuant))) {
            case attack_crit:
                ++crits;
                damageDealt+=critDmg;
            case attack_hit:
                ++successes;
                damageDealt+=atkDmg;
                continue;
            case attack_thwarted:
                ++thwarts;
            case attack_missed:
            default:
                ++misses;
        }
    }
    return report_test_results(attempts, successes, crits, misses, thwarts, damageDealt);
}

int proceed_with_valid_attacker(const int argc, const char **argv,
                                const unsigned int attackerDesiredDieFace,
                                const unsigned int attackerDiceQuantity) {
    int i, lastArg=argc-1, attackerHasCleave=0;
    unsigned int maxAttempts=DEFAULT_MAX_ATTEMPTS,
        attackerDamage=0,
        critDamage=0,
        defenderSuccessFace=FACE_CRIT,
        defenseDice=0;
    for(i=lastArg;i>0;--i)
        if(strcmp(argv[i], "-c")==0) { attackerHasCleave = 1; break; }
    for(i=lastArg-1;i>0;--i) {
        if(strcmp(argv[i], "-m")==0) {
            maxAttempts = numeric(argv[i+1]);
            continue;
        }
        if(strcmp(argv[i], "-d")==0) {
            attackerDamage = numeric(argv[i+1]);
            continue;
        }
        if(strcmp(argv[i], "-!")==0) {
            critDamage = numeric(argv[i+1]);
            continue;
        }
        if(strcmp(argv[i], "-D")==0) {
            defenderSuccessFace = SUCCESS_VIA_DODGE;
            defenseDice = numeric(argv[i+1]);
            continue;
        }
        if(strcmp(argv[i], "-G")==0) {
            defenderSuccessFace = (attackerHasCleave) ? SUCCESS_VIA_GUARD_BUT_CLEAVE : SUCCESS_VIA_GUARD;
            defenseDice = numeric(argv[i+1]);
            continue;
        }
        if(strcmp(argv[i], "-S")==0) {
            defenderSuccessFace = (attackerHasCleave) ? SUCCESS_VIA_SHIELD_BUT_CLEAVE : SUCCESS_VIA_SHIELDS;
            defenseDice = numeric(argv[i+1]);
            continue;
        }
    }
    return proceed_with_test(maxAttempts,
                             attackerDesiredDieFace,
                             attackerDiceQuantity,
                             attackerDamage,
                             critDamage,
                             defenderSuccessFace,
                             defenseDice);
}

int main(int argc, char **argv) {
    int i, lastArg=argc-1;
    for(i=lastArg;i>0;--i) {
        if(strcmp(argv[i], "-h")==0 && i<lastArg)
            return proceed_with_valid_attacker(argc, (const char **)argv,
                                     SUCCESS_VIA_HAMMERS,
                                     numeric(argv[i+1]));
        if(strcmp(argv[i], "-s")==0 && i<lastArg)
            return proceed_with_valid_attacker(argc, (const char **)argv,
                                     SUCCESS_VIA_SWORDS,
                                     numeric(argv[i+1]));
        if(strcmp(argv[i], "-?")==0)
            return show_help(argv[0], NULL);
    }
    return show_help(argv[0], "Please specify -h or -s for the attack type.");
}
