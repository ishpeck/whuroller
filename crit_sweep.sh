#!/bin/sh

for critdam in `jot 5 0`; do
    for damage in `jot 12`; do
        for shields in `jot 6`; do
            for hammers in `jot 12`; do
                echo "Attacking with $hammers hammers on cleave versus $shields shields ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $hammers -S $shields
            done
        done

        for shields in `jot 6`; do
            for swords in `jot 12`; do
                echo "Attacking with $swords swords on cleave versus $shields shields ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $swords -S $shields
            done
        done

        for dodge in `jot 6`; do
            for hammers in `jot 12`; do
                echo "Attacking with $hammers hammers on cleave versus $dodge dodge ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $hammers -D $dodge
            done
        done

        for dodge in `jot 6`; do
            for swords in `jot 12`; do
                echo "Attacking with $swords swords on cleave versus $dodge dodge ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $swords -D $dodge
            done
        done

        for guard in `jot 6`; do
            for hammers in `jot 12`; do
                echo "Attacking with $hammers hammers on cleave versus $guard dice on guard ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $hammers -G $guard
            done
        done

        for guard in `jot 6`; do
            for swords in `jot 12`; do
                echo "Attacking with $swords swords on cleave versus $guard dice on guard ($damage damage [+$critdam on crits])."
                ./roller_test -c -! $critdam -d $damage -h $swords -G $guard
            done
        done
    done | tee ./critresults-0${critdam}bonus
done
