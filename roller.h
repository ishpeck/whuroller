#ifndef _WHUROLLER_HEADER_FILE_
#define _WHUROLLER_HEADER_FILE_

#include <stdio.h>
#include <stdlib.h>

#define NUMBER_OF_DICE_FACES 6
#define toss_die() arc4random_uniform(NUMBER_OF_DICE_FACES)

typedef struct {
    int crits;
    int successes;
} roll_result;

typedef unsigned int roll_type;
#define SUCCESS_VIA_SWORDS 1
#define SUCCESS_VIA_HAMMERS 2

#define SUCCESS_VIA_DODGE 1
#define SUCCESS_VIA_SHIELDS 2
#define SUCCESS_VIA_GUARD 3

#define SUCCESS_VIA_SHIELD_BUT_CLEAVE 0
#define SUCCESS_VIA_GUARD_BUT_CLEAVE 1

/* Swirls */
#define CAST_VIA_FOCUS 2
/* Lightning */
#define CAST_VIA_CHANNEL 3

#define FACE_CRIT 0

typedef enum {
    attack_crit,
    attack_hit,
    attack_thwarted,
    attack_missed,
} attack_result;

roll_result roll(const unsigned int, const roll_type);
attack_result judge_attack(const roll_result, const roll_result);

#endif
