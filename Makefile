test: roller_test
	./roller_test || true 
	./roller_test 98023174 || true 
	./roller_test -h 2 || true
	./roller_test -h 2 -S 1 -m 20
	./roller_test -h 2 -D 1 -m 20
	./roller_test -h 2 -D 1 -c -m 20
	./roller_test -h 2 -S 1 -c -m 20
	./roller_test -h 2 -G 1 -c -m 20
	./roller_test -h 2 -G 1 -c
	./roller_test -h 2 -d 3 -G 1 -c
	./roller_test -s 3 -d 2 -G 1 -c
	./roller_test -! 2 -s 3 -d 2 -G 1 -c

roller_test: roller_test.c roller.c roller.h
	gcc -o roller_test roller_test.c roller.c

rollresults: sweeping_test.sh roller_test Makefile
	time ./sweeping_test.sh | tee rollresults
